import numpy as np
import csv
from FreeCAD import Vector, Rotation, Placement

def readfile2vecs(fname, scale = 1):
    '''Opens an xyz csv file (fname) and reads in as a list of vectors for each section'''
    k = list(csv.reader(open(fname)))
    wingmeshuvudivisions = eval(k[0][-3])
    assert (wingmeshuvudivisions == len(k[0])/3-1), 'Section numbering incorrect'
    vecs = []
    for i in range(0, (wingmeshuvudivisions*3)+2, 3):
        sec = []
        for j in range(2, len(k)):
            sec.append(Vector(float(k[j][i])*scale, float(k[j][i+1])*scale, float(k[j][i+2])*scale))
        vecs.append(sec)
    return vecs

def findLEind(sec):
    '''Find the index position of the LE highlight for a section'''
    LEind = False
    for i in range(1,len(sec)):
        if not LEind and sec[i].x>sec[i-1].x:
            LEind = i-1
            break
    return LEind

def flattenSec(pts, LEind, minthick = 0):
    """Function to give list of vectors in xz plane for aerofoil and rotation function to restore to normal position when given a list of points and the index of the Leading Edge point in the list (Doesn't normalise by chord) If minthick specified then upper points are rotated to move TE point up by 0.5*minthick and lower points rotated down (from nose)"""
    ords = []
    vLT = pts[LEind] - pts[0]
    hmax = 0
    hpt = None
    for pt in pts:
        vLP = pts[LEind] - pt
        lam = Vector.dot(vLT,vLP)/(vLT.Length**2)
        d = lam * vLT.Length
        h = vLP.Length**2 - d**2
        if h > hmax:
            hmax = h
            hpt = pt # Highest point on the profile
        if h > 0:
            h = np.sqrt(h)
        else:
            h = 0
        y = Vector.cross(vLP,vLT).y
        if y != 0:
            sign = abs(y)/y
        else:
            sign = 0
        ords.append(Vector(lam*vLT.Length,0,sign*h))
    if minthick != 0:
        a = np.degrees(np.arctan((minthick/2)/vLT.Length))
        #print('ords before',ords)
        rot = Rotation(Vector(0,1,0), -a)
        upperOrds = [rot*p for p in ords[:LEind]]
        rot = Rotation(Vector(0,1,0), a)
        lowerOrds = [rot*p for p in ords[LEind:]]
        ords = upperOrds + lowerOrds

    vLH = pts[LEind] - hpt #Vector from LE to a point near the highest point on the profile
    vPRx = -vLT
    vPRy = -Vector.cross(vLH,vPRx) # Vector normal to profile (rotate y axis onto this)
    vPRz = -Vector.cross(vPRx, vPRy) # Vector in plane with profile normal to chord (rotate z axis onto this)
    rot = Rotation(vPRx,vPRy,vPRz, 'YXZ')
    return ords, rot

def makeDSsection(pts, LEind, DSind = None, thick = 0.002):
    '''Function to make single surface area at trailing edge with a Double Surface area in front of this. If an index for the point at which the surfaces join is supplied then this is used, otherwise it is calculated to happen at a maximum thickness between upper and lower surfaces (as a proportion of chord)'''
    c = (pts[0]-pts[LEind]).Length
    if DSind == None:
        for i in range(len(pts)-1, int(len(pts)/2),-1):
            vTB = pts[i] - pts[-i-1]
            #print(vTB.Len()/c)
            if vTB.Length/c < thick:
                dsp = pts[i]
                DSind = i
    xs = []
    r = (pts[DSind]-pts[LEind]).x/(pts[0]-pts[LEind]).x
    #print(r)
    xnews = []
    for pt in pts[LEind:]:
        xd = pt.x - pts[LEind].x
        x = xd*r + pts[LEind].x
        xnews.append(x)
    xs = [pt.x for pt in pts[LEind:]]
    zs = [pt.z for pt in pts[LEind:]]
    znews = np.interp(xnews,xs,zs)
    uspts = [Vector(x,0,z) for x,z in zip(xnews,znews)]
    newpts = (pts[0:LEind]+uspts)
    return newpts, DSind

def loadwinggeometry(fname, scale = 1, DSinds = None, DSthick=0.002, scaleTH=None, minthick=0.001):
    '''Load a wing geometry from an xyz style csv file, optionally applying a scale factor and adding a double surface and single surface area based on either index or thickness. Pass DSinds = None to not apply a double surface correction, DSinds = [] to calculate DSinds based on the thickness or DSinds = [List of index values of seam between top and bottom surface equal in length to number of sections] to use the index values. Also can optionally scale section along the camber line and set a minimum thickness in absolute terms to make a better model wing'''
    surf = readfile2vecs(fname,scale)
    LEind = findLEind(surf[0])
    print('LEind',LEind, surf[0][LEind])
    if DSinds:
        if len(DSinds) != len(surf):
            DSinds = []
    sections = []
    xvals, yvals, zvals, rots = [], [], [], []
    inSail = []
    for i in range(len(surf)):
        sec = surf[i]
        y = sec[0].y
        if (y != sec[int(len(sec)/3)].y):
            print('Wing section rotated')
            r = True
            inSail.append(False)
        else:
            r = False
            inSail.append(True)
        
        pts, rot = flattenSec(sec, LEind)
        if DSinds:
            if len(DSinds) == len(surf):
                pts, DSind = makeDSsection(pts, LEind, DSinds[i], thick = DSthick)
            else:
                pts, DSind = makeDSsection(pts, LEind, None, thick = DSthick)
                DSinds.append(DSind)
        if scaleTH:
            pts = scaleSection(pts, scaleTH, LEind, minthick)
        
        xvals.append(sec[LEind].x)
        yvals.append(sec[LEind].y)
        zvals.append(sec[LEind].z)
        sections.append(pts)
        rots.append(rot)
    return {'sections':sections, 'xvals':xvals,'yvals':yvals, 'zvals':zvals, 'rots':rots, 'inSail':inSail, 'DSinds':DSinds}

def scaleSection(sec, scaleTH, LEind, minthick = 0):
    '''Scales section for use in a model. Will scale section while keeping the same camber line and also enforce a minimum absolute thickness. Note, this resamples the under surface points so that they will lie at a corresponding x position to the one on the upper surface'''
    upper = sec[:LEind]
    lower = sec[LEind:]
    upper_scaled = []
    lower_scaled = []
    
    for pt in upper:
        ptL1 = lower[0]
        for i in range(len(lower)):
            ptL2 = lower[i]
            if ptL1.x<=pt.x<ptL2.x:
                break
            ptL1 = ptL2
        if ptL1.x == ptL2.x:
            r = 0
        else:
            r = (pt.x-ptL1.x) / (ptL2.x-ptL1.x)
        v = ptL2-ptL1
        ptL = ptL1 + v*r
        #print(ptL1,ptL,ptL2,r)
        ptC = pt - (pt-ptL)*0.5 #point on camber line
        ptUsc = Vector((pt.x),0,(ptC.z + max(minthick/2, scaleTH*(pt.z-ptC.z))))
        ptLsc = Vector((ptL.x),0,(ptC.z - max(minthick/2, scaleTH*(ptC.z-ptL.z))))
        upper_scaled.append(ptUsc)
        lower_scaled.insert(0,ptLsc)
    scaled = upper_scaled + [sec[LEind]] + lower_scaled      
    return scaled

#Function to mesh between two aero-profiles
def foil_mesh(points1, points2, trianglepoints):
	assert len(points1) == len(points2), 'Can only mesh between foils of equal number of points'
	trianglepoints = trianglepoints
	for i in range(len(points1)-1):
		ip = (i+1)
		trianglepoints.append(points1[i]);  trianglepoints.append(points2[i]);   trianglepoints.append(points2[ip]); 
		trianglepoints.append(points1[i]);  trianglepoints.append(points2[ip]);  trianglepoints.append(points1[ip]); 
	return trianglepoints
