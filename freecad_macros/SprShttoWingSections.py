# -*- coding: utf-8 -*-
# Macro to create a mesh for a hang glider wing suitable for flattening to produce sailmaking patterns
#V0.0
	#-Basic functionality creates single mesh for single aerofoil data file and set of wing sweeps, twists and dihedrals
	#-just uses base sections, no interpolation
#V0.1
	# Added linear interpolation of aerofoil sections

version = 0.1

import Draft
import DraftGeomUtils
import math
import Part
from FreeCAD import Vector
import Mesh
import numpy as np
import sys
import json

sys.path.append(os.path.split(__file__)[0])
from p7modules.p7wingeval import getemptyobject, createobjectingroup, removeObjectRecurse, getemptyfolder

#Function to mesh between two aero-profiles
def foil_mesh(points1, points2, trianglepoints):
	trianglepoints = trianglepoints
	for i in range(len(points1)-1):
		ip = (i+1)
		trianglepoints.append(points1[i]);  trianglepoints.append(points2[i]);   trianglepoints.append(points2[ip]); 
		trianglepoints.append(points1[i]);  trianglepoints.append(points2[ip]);  trianglepoints.append(points1[ip]); 
	return trianglepoints

def interpolate_ang(ang, axs,d = 100):
	xs, ys = [ 0 ], [ 0 ]
	#axs = [semi_span*i/(len(ang)-1)  for i in range(len(ang))]
	#print(axs)
	for i in range(1, len(ang)):
		x0, x1 = axs[i-1], axs[i]
		a0, a1 = ang[i-1], ang[i]
		n = int((x1-x0)/d) + 1
		for j in range(1, n+1):
			x = x0 + (x1-x0)*j/n
			a = a0 + (a1-a0)*j/n
			slope = math.tan(math.radians(a))
			dy = slope*(x-xs[-1])
			xs.append(x)
			ys.append(ys[-1]+dy)
	return xs,ys

#Function to interpolate items at various points
def interpolate_length(lengths, secs, interp):
	points = []

	for station in range (0, len(lengths)):
		points.append(FreeCAD.Vector(secs[station], lengths[station],0))

	BSpline = Part.BSplineCurve(points)
	wire = DraftGeomUtils.curvetowire(BSpline,interp)
	ys = [lengths[0]]

	for edge in wire:
		ys.append(edge.lastVertex().Y)
	return ys

def interpolate_section(r,secpts,rs):
	assert len(secpts) == len(rs), 'Must a position for every section'
	for pts in secpts:
		assert len(pts) == len(secpts[0]), 'sections must contain same number of points'
	OPpts = []
	for i in range(len(secpts[0])):
		ys = []
		for j in range(len(secpts)):
			assert secpts[j][i][0] == secpts[0][i][0], 'sections must have matching x ordinates'
			ys.append(secpts[j][i][1])
		y = np.interp(r, rs, ys)
		OPpts.append((secpts[0][i][0], y, 0))
	return OPpts

def angToPlacement(angs,secs):
	ys = [0]
	for i in range(len(angs)):
		slope = math.tan(math.radians(angs[i]))
		dy = slope*(secs[i+1]-secs[i])
		ys.append(ys[-1]+dy)
	return ys

def convertDS(pts,ds, seam,chord):
	l=len(pts)
	aerof=[]
	dsp = [ds,0]
	dsfp = [seam,0]
	found = False
	front = False
	pl = pts[0]
	highlight=False
	
	for p in pts:
		if ds == chord:
			dsp[1] = 0
			found = True
		elif p[0]*chord < ds and not found:
			dsp[1] = p[1]*chord + ((ds-p[0]*chord)*(pl[1]-p[1]))/(pl[0]-p[0])
			found = True
			#print(dsp)
		if not highlight and p[0]>pl[0]:
			highlight = True
			#print('found highlight',pl)
		if highlight and p[0]*chord>seam:
			dsfp[1]= p[1]*chord + ((seam-p[0]*chord)*(pl[1]-p[1]))/(pl[0]-p[0])
			#print('foundseam',p[0]*chord)
			BSptsx=np.linspace(p[0]*chord,dsp[0],l-len(aerof))
			for x in BSptsx:
				z=np.interp(x,[dsfp[0],dsp[0]],[dsfp[1],dsp[1]])
				aerof.append(App.Vector(0,-x,z))
			break
		aerof.append(App.Vector(0,-p[0]*chord,p[1]*chord))
		pl = p
	return aerof,dsp
		
dirname = os.path.dirname(os.path.abspath(__file__))
fname = '../data/aerofoils/ui1720reflex.csv'
#fname = '../data/aerofoils/e193.csv'
root_file = os.path.join(dirname, fname)
print('root section:', root_file)
#fname = '../data/aerofoils/2R15section.csv'
fname = '../data/aerofoils/ui1720mod.csv'
mid_file = os.path.join(dirname, fname)
print('mid section:', mid_file)
#fname = '../data/aerofoils/2R15section.csv'
#fname = '../data/aerofoils/e193.csv'
outb_file = os.path.join(dirname, fname)
print('outb section:', outb_file)
fname = '../data/aerofoils/tipsection.csv'
#fname = '../data/aerofoils/e193.csv'
tip_file = os.path.join(dirname, fname)
print('tip section:', tip_file)

doc = App.ActiveDocument
ss = doc.Spreadsheet

secs = json.loads(ss.get("secs"))
semi_span = secs[-1]

sweeps = angToPlacement(json.loads(ss.get("sweeps")),secs)
dihedrals = angToPlacement(json.loads(ss.get("dihedrals")), secs)
chords = json.loads(ss.get("chords"))
twists = json.loads(ss.get("twists"))
doubleSurfs = json.loads(ss.get("double_surf_dist"))
seamDSs = json.loads(ss.get("seamDS"))

trianglepoints = [ ]

rootpts = np.loadtxt(root_file,delimiter = ',')
midpts = np.loadtxt(mid_file,delimiter = ',')
outbpts = np.loadtxt(outb_file,delimiter = ',')
tippts = np.loadtxt(tip_file,delimiter = ',')
secpts = [rootpts,midpts,outbpts,tippts]
rs = [0,0.5,0.75,1]
dsl=[]

Vparameter_wingsectionL = "Vparameter_wingsect"

Vparameter_wingsection = None
objs = doc.findObjects(Label=Vparameter_wingsectionL)
if objs:
	Vparameter_wingsection = doc.copyObject(objs[0])
	print("CnewVsection ", Vparameter_wingsection.Name, Vparameter_wingsection.Label)

wingsectionsgroup = getemptyfolder(doc, "WingSections")
Isection = 5

for station in range(len(secs)):
	pts = interpolate_section((secs[station]/secs[-1]), secpts, rs)
	points, dsp = convertDS(pts, doubleSurfs[station], seamDSs[station], chords[station])
	points[-1].y -= 0.001  # if points match then closed flag is ignored and it closes it anyway
	section = Draft.makeWire(points, closed=False, face=False)
	section.Label = "wingsection_%d" % station
	section.adjustRelativeLinks(wingsectionsgroup)
	wingsectionsgroup.addObject(section)
	print("newsection ", section.Name, section.Label)
	
	section.Placement = App.Placement(App.Vector(secs[station],-sweeps[station], dihedrals[station]), App.Rotation(App.Vector(1,0,0),twists[station]), App.Vector(0,0,0))

	if Vparameter_wingsection == None and station == Isection:
		Vparameter_wingsection = doc.copyObject(section)
		print("newVsection ", Vparameter_wingsection.Name, Vparameter_wingsection.Label)

	points = [section.Placement*p  for p in section.Points]
	dsl.append(section.Placement*App.Vector(0,-dsp[0],dsp[1]))
	if station > 0:
		trianglepoints = foil_mesh(last_points,  points, trianglepoints)
	last_points = points

trailingline = Draft.makeWire(dsl, closed=False)
trailingline.Label = "doublesurfaceline"
trailingline.adjustRelativeLinks(wingsectionsgroup)
wingsectionsgroup.addObject(trailingline)

fmesh = createobjectingroup(doc, wingsectionsgroup, "Mesh::Feature", "loftedwingsurface")
fmesh.Mesh = Mesh.Mesh(trianglepoints)

print(Vparameter_wingsection.Name, Vparameter_wingsection.Label)
Vparameter_wingsection.Label = Vparameter_wingsectionL
print(Vparameter_wingsection.Name, Vparameter_wingsection.Label)

Vparameter_wingsection.adjustRelativeLinks(wingsectionsgroup)
wingsectionsgroup.addObject(Vparameter_wingsection)

doc.recompute([wingsectionsgroup])
NVwingsecVerts = len(Vparameter_wingsection.Shape.OrderedVertexes)
for lsection in wingsectionsgroup.OutList:
	if lsection.Label.startswith("wingsection_"):
		NsecVerts = len(lsection.Shape.OrderedVertexes)
		assert NsecVerts == NVwingsecVerts, ("Error: wingsection has different number of nodes to Vparameter_wingsection", NsecVerts, NVwingsecVerts)

