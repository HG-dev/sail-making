import FreeCAD as App
import Draft
import os, csv
import numpy as np
from FreeCAD import Vector, Rotation
from p7modules.p7wingeval import getemptyfolder
from p7modules.p7utils import loadwinggeometry

sys.path.append(os.path.split(__file__)[0])

p7csvfile = "/home/timbo/myrepos/gitlab/wildcat/Raw Geometry/P7Wildcat-240924.csv"
p7csvfile = os.path.join(os.path.dirname(os.path.abspath(__file__)), p7csvfile)

doc = App.ActiveDocument

LEgroup = getemptyfolder(doc, "Leading edges")

wingGeom = loadwinggeometry(p7csvfile)

crankInd = 8
LEIind = 21
tipInd = 26

print('Y ordinate of crank', wingGeom['yvals'][crankInd])
pts = [Vector(x,y,z)
       for x,y,z in zip(wingGeom['xvals'][:crankInd+1],
                        wingGeom['yvals'][:crankInd+1],
                        wingGeom['zvals'][:crankInd+1])]

LE = Draft.makeWire(pts, closed = False)
LE.Label = "LE to crank Highlight"
LE.adjustRelativeLinks(LEgroup)
LEgroup.addObject(LE)
totLen = 0
for i in range(1,len(pts)):
    v = pts[i] - pts[i-1]
    totLen += v.Length
print('Length along highlight to crank:', totLen)

print('Y ordinate of end of inner LE',wingGeom['yvals'][LEIind])
pts = [Vector(x,y,z)
       for x,y,z in zip(wingGeom['xvals'][crankInd:LEIind+1],
                        wingGeom['yvals'][crankInd:LEIind+1],
                        wingGeom['zvals'][crankInd:LEIind+1])]

LE = Draft.makeWire(pts, closed = False)
LE.Label = "Crank to end of LEI Highlight"
LE.adjustRelativeLinks(LEgroup)
LEgroup.addObject(LE)
totLen = 0
for i in range(1,len(pts)):
    v = pts[i] - pts[i-1]
    totLen += v.Length
print('Length along highlight from crank to end of LEI:', totLen)

print('Y ordinate of tip',wingGeom['yvals'][tipInd])
pts = [Vector(x,y,z)
       for x,y,z in zip(wingGeom['xvals'][LEIind:tipInd+1],
                        wingGeom['yvals'][LEIind:tipInd+1],
                        wingGeom['zvals'][LEIind:tipInd+1])]

LE = Draft.makeWire(pts, closed = False)
LE.Label = "End of LEI to tip Highlight"
LE.adjustRelativeLinks(LEgroup)
LEgroup.addObject(LE)
totLen = 0
for i in range(1,len(pts)):
    v = pts[i] - pts[i-1]
    totLen += v.Length
print('Length along highlight from end of LEI to tip:', totLen)

doc.recompute()