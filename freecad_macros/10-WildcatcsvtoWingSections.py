# -*- coding: utf-8 -*-
# Macro to make project7wing as list of wires and single bspline surface

import FreeCAD as App
import Draft, Mesh
import os, csv
import numpy as np
from FreeCAD import Vector, Rotation
from p7modules.p7wingeval import getemptyfolder, createobjectingroup
from p7modules.barmesh.basicgeo import P2
from p7modules.p7utils import loadwinggeometry, foil_mesh

sys.path.append(os.path.split(__file__)[0])

#p7csvfile = "/home/timbo/myrepos/gitlab/wildcat/Raw Geometry/P7U-230324.csv"
#p7csvfile = "/home/timbo/myrepos/gitlab/wildcat/Raw Geometry/P7Wildcat-240924.csv"
p7csvfile = "D:/Documents/Hang Gliding/Manufacturing-Technical/Avian Project/P7Wildcat/wildcat-main/Raw Geometry/P7Wildcat-240924.csv"
p7csvfile = os.path.join(os.path.dirname(os.path.abspath(__file__)), p7csvfile)

doc = App.ActiveDocument
try:
    ss = doc.getObject("Spreadsheet")
    ss.DSinds
    print('SS',ss)
    print('DSinds', DSinds, type(DSinds)) # I CAN'T WORK OUT WHY THIS ISN'T WORKING
except:
    DSinds = None
DSinds = [54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,60,60,60,60,60]
wingsectionsgroup = getemptyfolder(doc, "WingSections")

Vparameter_wingsectionL = "Vparameter_wingsect"

Vparameter_wingsection = None
objs = doc.findObjects(Label=Vparameter_wingsectionL)
if objs:
	Vparameter_wingsection = doc.copyObject(objs[0])
	print("CnewVsection ", Vparameter_wingsection.Name, Vparameter_wingsection.Label)

#IF MAKING SCALE MODEL!!!

model = False

if model:
    scale = 0.035
    wingGeom = loadwinggeometry(p7csvfile, scale, scaleTH=1, minthick=0.4 ) # TODO: scaleTH introducing kink at LE, need to fix if making flying model
else:
    scale = 1
    wingGeom = loadwinggeometry(p7csvfile, scale, DSinds = DSinds, DSthick=0.002)
    print('DSinds:', wingGeom['DSinds'])
print('Wing loaded with:', len(wingGeom['sections']))
print('y stations:', wingGeom['yvals'])
trianglepoints = [ ]

Isection = 7

for station in range(len(wingGeom['yvals'])):
    pts = wingGeom['sections'][station]
    pts[-1] = pts[-1] - Vector(0,0,0.001)
    section = Draft.makeWire(pts, closed = not wingGeom['inSail'][station])
    section.Label = "wingsection_%d" % station
    section.adjustRelativeLinks(wingsectionsgroup)
    wingsectionsgroup.addObject(section)
    print("newsection ", section.Name, section.Label)
    pp = Vector(wingGeom['xvals'][station], wingGeom['yvals'][station], wingGeom['zvals'][station])
    section.Placement = App.Placement(pp, wingGeom['rots'][station], App.Vector(0,0,0))
    
    if Vparameter_wingsection == None and station == Isection:
        Vparameter_wingsection = doc.copyObject(section)
        print("newVsection ", Vparameter_wingsection.Name, Vparameter_wingsection.Label)
    doc.recompute()
    if wingGeom['inSail'][station]:
        points = [v.Point for v in section.Shape.OrderedVertexes]
        print('NUMBER OF POINTS', len(pts),len(points))
        if station > 0:
            trianglepoints = foil_mesh(last_points, points, trianglepoints)
        last_points = points

fmesh = createobjectingroup(doc, wingsectionsgroup, "Mesh::Feature", "loftedwingsurface")
fmesh.Mesh = Mesh.Mesh(trianglepoints)

print(Vparameter_wingsection.Name, Vparameter_wingsection.Label)
Vparameter_wingsection.Label = Vparameter_wingsectionL
print(Vparameter_wingsection.Name, Vparameter_wingsection.Label)

Vparameter_wingsection.adjustRelativeLinks(wingsectionsgroup)
wingsectionsgroup.addObject(Vparameter_wingsection)

doc.recompute([wingsectionsgroup])
NVwingsecVerts = len(Vparameter_wingsection.Shape.OrderedVertexes)
for lsection in wingsectionsgroup.OutList:
    if lsection.Label.startswith("wingsection_") and not lsection.Closed:
        print(lsection.Label, type(lsection))
        NsecVerts = len(lsection.Shape.OrderedVertexes)
        assert NsecVerts == NVwingsecVerts, ("Error: wingsection has different number of nodes to Vparameter_wingsection", NsecVerts, NVwingsecVerts)

