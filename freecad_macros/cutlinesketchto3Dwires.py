# -*- coding: utf-8 -*-

# This macro is for generating a set of PatchUVPolygons (polygons in UV space)
# derived from the cutlinesketch diagram, which are to be used as the basis of 
# the flattening, offsetting and trimming 

import FreeCAD as App
import Draft, Part, Mesh
import DraftGeomUtils
import math, os, csv, sys
import numpy
from FreeCAD import Vector, Rotation

sys.path.append(os.path.split(__file__)[0])
from p7modules.p7wingeval import WingEval
from p7modules.p7wingeval import getemptyobject, createobjectingroup, removeObjectRecurse, makeclearedsketch, getemptyfolder

doc = App.ActiveDocument

cutlinesketch = doc.findObjects(Label="cutlinesketch")[0]
penmarkssketch = doc.findObjects(Label="penmarkssketch")[0]

wingeval = WingEval(doc, True)
urange, vrange = wingeval.urange, wingeval.vrange

cutlinewiresfolder = getemptyfolder(doc, "Scutlinewires")
penmarkwiresfolder = getemptyfolder(doc, "Spenmarkwires")

legsampleleng = 3.0

sketchwirescolours = [ 
	(cutlinesketch, cutlinewiresfolder, (0.0, 0.0, 1.0), "clw_%d"), 
	(penmarkssketch, penmarkwiresfolder, (0.5, 0.0, 1.0), "pmw_%d") 
]

for sketch, wiresfolder, colour, lab in sketchwirescolours:
	for i, x in enumerate(sketch.GeometryFacadeList):
		if not x.Construction:
			points = [ ]
			l = sketch.Geometry[i]
			num = int(math.ceil(l.length()/legsampleleng) + 1)
			params = numpy.linspace(l.FirstParameter, l.LastParameter, num)
			for a in params:
				p = l.value(a)
				points.append(wingeval.seval(p.x, p.y))
			wire = Draft.makeWire(points, closed=False)
			wire.Label = lab % i
			wire.adjustRelativeLinks(cutlinewiresfolder)
			print(colour)
			wire.ViewObject.LineColor = colour
			wire.ViewObject.PointColor = wire.ViewObject.LineColor
			wire.ViewObject.LineWidth = 3.0
			wiresfolder.addObject(wire)

