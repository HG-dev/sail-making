# -*- coding: utf-8 -*-

# This macro takes the areas out of PatchUVPolygons (polygons in UV space)
# and trims sections of the wingsurface to them to generate triangulated surfaces

import FreeCAD as App
import Draft, Part, Mesh, Fem
import os, sys, math, time
import numpy
from FreeCAD import Vector, Rotation
import flatmesh
from PySide import QtGui, QtCore

sys.path.append(os.path.join(os.path.split(__file__)[0]))
print(sys.path[-1])
from p7modules.barmesh.basicgeo import I1, Partition1, P3, P2, Along, lI1
# Do this if running by pasting into Python window
#sys.path.append("/home/julian/repositories/HGnotebooks/wingflattening/freecad_macro_work")
#sys.path.append(os.path.split(__file__)[0])
from p7modules.p7wingeval import getemptyfolder, createobjectingroup

doc = App.ActiveDocument

def okaypressed():
    striangulations = doc.STriangulations.OutList
    sflattened = doc.SFlattened.OutList
    
    print("Okay Pressed") 
    colv0 = float(qvaluelo.text())
    colv2 = float(qvaluehi.text())
    col0 = P3(*[float(x.strip())  for x in qcol0.text().split(",") ])
    col1 = P3(*[float(x.strip())  for x in qcol1.text().split(",") ])
    col2 = P3(*[float(x.strip())  for x in qcol2.text().split(",") ])
    colv1 = (colv0 + colv2)/2
    print("colv", colv0, colv1, colv2)
    def convcl(v, v0, v1, cl0, cl1):
        lam = max(0, min(1, (v - v0)/(v1 - v0)))
        c = cl0*(255*(1-lam)) + cl1*(255*lam)
        c = cl0*(255*(1-lam)) + cl1*(255*lam)
        return (int(c[0])<<24) + (int(c[1])<<16) + (int(c[2])<<8) + 255
    def convcol(v):
        return convcl(v, colv0, colv1, col0, col1) if v < colv1 else convcl(v, colv1, colv2, col1, col2)
    
    baspectratio = qaspectareacheck.isChecked()
    print("Comparing", "aspect" if baspectratio else "area", "ratio")

    cs = [ ]
    
    for i, (mtriangulation, mflattened) in enumerate(zip(striangulations, sflattened)):
        facets1 = mtriangulation.Mesh.Facets
        facets2 = mflattened.Mesh.Facets
        assert(len(facets1) == len(facets2))
        FaceColors = [ ]
        for f1, f2 in zip(facets1, facets2):
            if baspectratio:
                c = f1.AspectRatio/f2.AspectRatio - 1.0
            else:
                c = f1.Area/f2.Area - 1.0
            cs.append(c)
            FaceColors.append(convcol(c))
        print(mtriangulation.Label, " distort range %.4f, %.4f" % (min(cs), max(cs)))
        if "FaceColors" not in mtriangulation.PropertiesList:
            mtriangulation.addProperty("App::PropertyColorList", "FaceColors")
        mtriangulation.FaceColors = FaceColors
        if "FaceColors" not in mflattened.PropertiesList:
            mflattened.addProperty("App::PropertyColorList", "FaceColors")
        mflattened.FaceColors = FaceColors
        
    qw.hide()

    print("To render the colours, right click on the mesh in the tree view")
    print("Until you get a context sensitive menu option: [ ] Display colors")
    print("Must be manually turned off and on if values are changed")
    print("since we cannot access ViewProviderMesh::setColorPerFace()")
    
    # we should be able to make this easier by doing it with the property
    # mflattened.ViewObject.addProperty("App::PropertyColorList", "distortcols")
    # and setting the Coloring option in the View settings for the mesh, 
    # and rely on tryColorPerVertexOrFace(), but it does not seem to work!

def qrow(qw, slab, yval, txt="", xposoffs=0):
    v = QtGui.QLineEdit(qw); 
    v.move(120+xposoffs, yval); 
    vlab = QtGui.QLabel(slab, qw)
    vlab.move(20+xposoffs, yval+5)
    v.setText(txt)
    return v

qw = QtGui.QWidget()
qw.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
qw.setGeometry(700, 500, 300, 350)
qw.setWindowTitle('Measure thickness')
#qmeshes = qrow(qw, "Meshes: ", 15+35*1, "Striangulations/SFlattened")
qvaluelo = qrow(qw, "ValueLo: ", 15+35*2, "-0.01")
qvaluehi = qrow(qw, "ValueHi: ", 15+35*3, "0.01")
qcol0 = qrow(qw, "Colour0: ", 15+35*4, "1,0,0")
qcol1 = qrow(qw, "Colour1: ", 15+35*5, "0.9,0.9,0.9")
qcol2 = qrow(qw, "Colour2: ", 15+35*6, "0,0,1")

qaspectareacheck = QtGui.QCheckBox("Aspect or Area ratio", qw)
qaspectareacheck.setChecked(True)
qaspectareacheck.move(30, 15+35*7)

okButton = QtGui.QPushButton("Measure", qw)
okButton.move(180, 15+35*8)
QtCore.QObject.connect(okButton, QtCore.SIGNAL("pressed()"), okaypressed)  

qw.show()


#obj.ViewObject.HighlightedNodes = [1, 2, 3]
#The individual elements of a mesh can be modified by passing a dictionary with the appropriate key:value pairs.
#Set volume 1 to red
#obj.ViewObject.ElementColor = {1:(1,0,0)}
#Set nodes 1, 2 and 3 to a certain color; the faces between the nodes acquire an interpolated color.
#obj.ViewObject.NodeColor = {1:(1,0,0), 2:(0,1,0), 3:(0,0,1)}
#Displace the nodes 1 and 2 by the magnitude and direction defined by a vector.
#obj.ViewObject.NodeDisplacement = {1:FreeCAD.Vector(0,1,0), 2:FreeCAD.Vector(1,0,0)}
