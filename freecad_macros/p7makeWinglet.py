import FreeCAD as App
import Draft, Mesh
import os, csv
import numpy as np
from FreeCAD import Vector, Rotation, Placement
from p7modules.p7wingeval import getemptyfolder, createobjectingroup
from p7modules.barmesh.basicgeo import P2
from p7modules.p7utils import readfile2vecs, findLEind, flattenSec, scaleSection

sys.path.append(os.path.split(__file__)[0])

p7csvfile = "/home/timbo/myrepos/gitlab/wildcat/Raw Geometry/P7Wildcat-240924.csv"
p7csvfile = os.path.join(os.path.dirname(os.path.abspath(__file__)), p7csvfile)

doc = App.ActiveDocument

wingsectionsgroup = getemptyfolder(doc, "Winglet")

scale = 1
wingletInd = 26
surf = readfile2vecs(p7csvfile,scale)
surf = surf[wingletInd:]
LEindRef = findLEind(surf[0])
ordRef, rotRef = flattenSec(surf[0], LEindRef)
cRef = (ordRef[0] - ordRef[LEindRef]).Length
print('Ref chord length', cRef)
cSpacingsTop = []
for pt in ordRef[:LEindRef]:
    cSpacingsTop.append(pt.x/cRef)
cSpacingsBot = []
for pt in ordRef[LEindRef:]:
    cSpacingsBot.append(pt.x/cRef)

LEpts, TEpts = [], []
    
for sec in surf:
    LEind = findLEind(sec)    
    minthick = 1
    ords, rot = flattenSec(sec, LEind, minthick)
    print('LEind before', LEind)

    # Workout chord line and mould runoffs
    LEpt = ords[LEind]
    TEpt = ords[0]-Vector(0,0,minthick/2)
    LEpts.append(LEpt)
    TEpts.append(TEpt)
    runoff = Vector(100,0,0)
    midpt = LEpt + (TEpt-LEpt)*0.5
    splitline = Draft.makeWire([LEpt-runoff,
                                LEpt,
                                midpt+ Vector(0,0,(TEpt-LEpt).Length*0.025),
                                TEpt,
                                TEpt+runoff], closed = False)
    splitline.Label = "chordline-ext"
    splitline.adjustRelativeLinks(wingsectionsgroup)
    wingsectionsgroup.addObject(splitline)
    
    c = (ords[0] - ords[LEindRef]).Length
    
    #Separate top and bottom surfaces and normalise to chord
    xnsTop = []
    znsTop = []
    for pt in ords[:LEind]:
        xnsTop.append(pt.x/c)
        znsTop.append(pt.z/c)
    xnsBot = []
    znsBot = []
    for pt in ords[LEind:]:
        xnsBot.append(pt.x/c)
        znsBot.append(pt.z/c)

    #Interpolate section sampling to match reference one

    #Scale section back up by c and recombine top and bottom

    #Check new LEind
    LEind = findLEind(ords)
    print('LEind after', LEind)

    if sec == surf[0]:
        secSpl = Draft.make_bspline(ords, closed = False)
        secSpl.Label = "wingletSpleen"
        secSpl.adjustRelativeLinks(wingsectionsgroup)
        wingsectionsgroup.addObject(secSpl)
        pp = sec[LEind] - Vector(0,30,0)
        secSpl.Placement = Placement(pp, rot, Vector(0,0,0))
        spleenline = Draft.makeWire([LEpt-runoff, LEpt, midpt, TEpt, TEpt+runoff], closed = False)
        spleenline.Label = "chordline-ext"
        spleenline.adjustRelativeLinks(wingsectionsgroup)
        wingsectionsgroup.addObject(spleenline)
        spleenline.Placement = Placement(pp, rot, Vector(0,0,0))
    
    section = Draft.makeWire(ords, closed = False)
    secSpl = Draft.make_bspline(ords, closed = False)
    section.Label = "wingletsec"
    secSpl.Label = "wingletSpl"
    section.adjustRelativeLinks(wingsectionsgroup)
    secSpl.adjustRelativeLinks(wingsectionsgroup)
    wingsectionsgroup.addObject(section)
    wingsectionsgroup.addObject(secSpl)
    pp = sec[LEind]
    section.Placement = Placement(pp, rot, Vector(0,0,0))
    secSpl.Placement = Placement(pp, rot, Vector(0,0,0))
    splitline.Placement = Placement(pp, rot, Vector(0,0,0))
