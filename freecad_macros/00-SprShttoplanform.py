import Draft, Part, Mesh, Sketcher
import DraftGeomUtils
import math, os, csv, json
from FreeCAD import Vector, Rotation
sys.path.append(os.path.split(__file__)[0])
from p7modules.p7wingeval import getemptyobject, createobjectingroup, removeObjectRecurse
	

doc = App.ActiveDocument
ss = doc.Spreadsheet
secs = json.loads(ss.secs)
chords = json.loads(ss.chords)
sweeps = json.loads(ss.sweeps)
DSdists = json.loads(ss.double_surf_dist)

dytot = 0
LEpts=[]
TEpts=[]
DSpts=[]

def makeclearedsketch(doc, lname):
	objs = doc.findObjects(Label=lname)
	obj = objs[0] if objs else doc.addObject("Sketcher::SketchObject", lname)
	obj.Label = lname
	obj.deleteAllGeometry()
	return obj

planformsketch = makeclearedsketch(doc, "planformsketch")

for i in range(0, len(secs)):
	if i==0:
		p0 = Vector(secs[i],0)
		p1 = Vector(secs[i],-chords[i])
		planformsketch.addGeometry(Part.LineSegment(p0, p1),False)
	else:
		slope = math.tan(math.radians(sweeps[i-1]))
		dy = slope*(secs[i]-secs[i-1])
		dytot = dytot+dy
		p0 = Vector(secs[i],-dytot)
		p1 = Vector(secs[i],-dytot-chords[i])
		if i == len(secs)-1:
			planformsketch.addGeometry(Part.LineSegment(p0, p1),False)
		else:
			planformsketch.addGeometry(Part.LineSegment(p0, p1),True)
	LEpts.append(p0)
	TEpts.append(p1)
	DSpts.append(Vector(secs[i],-dytot-DSdists[i]))
for i in range(1,len(LEpts)):
	planformsketch.addGeometry(Part.LineSegment(LEpts[i-1],LEpts[i]),False)
	planformsketch.addGeometry(Part.LineSegment(TEpts[i-1],TEpts[i]),False)
	planformsketch.addGeometry(Part.LineSegment(DSpts[i-1],DSpts[i]),True)
doc.recompute()

# Generate constraints from matching nodes
#WOULD BE NICE TO MAKE POL CONSTRAINTS ON DS VERTICES TO SECS
pointindexes = [ ]
for i, e in enumerate(planformsketch.Geometry):
	pointindexes.append((e.StartPoint.x, e.StartPoint.y, e.StartPoint.z, i, 1))
	pointindexes.append((e.EndPoint.x, e.EndPoint.y, e.EndPoint.z, i, 2))
	if e.StartPoint.x == e.EndPoint.x:
		planformsketch.addConstraint(Sketcher.Constraint("Vertical",i))
	if e.StartPoint.y == e.EndPoint.y:
		planformsketch.addConstraint(Sketcher.Constraint("Horizontal",i))
pointindexes.sort()

for i in range(len(pointindexes)-1):
	if pointindexes[i][:3] == pointindexes[i+1][:3]:
		#print( pointindexes[i][:3])
		planformsketch.addConstraint(Sketcher.Constraint("Coincident", pointindexes[i][3], pointindexes[i][4], pointindexes[i+1][3], pointindexes[i+1][4]))

doc.recompute()
