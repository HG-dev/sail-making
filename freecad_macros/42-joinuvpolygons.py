# -*- coding: utf-8 -*-

# This macro is for generating a set of PatchUVPolygons (polygons in UV space)
# derived from the cutlinesketch diagram, which are to be used as the basis of 
# the flattening, offsetting and trimming 

import FreeCAD as App
import Draft, Part, Mesh
import DraftGeomUtils
import math, os, csv, sys
import numpy
from FreeCAD import Vector, Rotation

sys.path.append(os.path.split(__file__)[0])
from p7modules.p7wingeval import WingEval
from p7modules.p7wingeval import createobjectingroup, getemptyfolder, getemptyobject
from p7modules.barmesh.basicgeo import P2

doc = App.ActiveDocument

PolyArray = []
PolyNames = doc.UVPolygons.getSubObjects()

Poly1Shape = doc.UVPolygons.getObject("wPoly4").Shape
Poly2Shape = doc.UVPolygons.getObject("wPoly5").Shape
P1 = Poly1Shape.Vertexes[-50].Point
P2 = Poly2Shape.Vertexes[0].Point
print("Poly1 num vertexes: ", len(Poly1Shape.Vertexes))
print("Poly2 num vertexes: ", len(Poly2Shape.Vertexes))
print("Poly1 num edges: ", len(Poly1Shape.Edges))
print("Poly2 num edges: ", len(Poly2Shape.Edges))
print("P1: ", P1)
print("P2: ", P2)
if P1 == P2:
	print("Match")
#for v1 in Poly1Shape.Vertexes:
#	for v2 in Poly2Shape.Vertexes:
#		print("v1: ", v1.X, v1.Y, v1.Z)
#		print("v2: ", v2.X, v2.Y, v2.Z)
#		if v1.Point == v2.Point:
#			print("Match!")
