# -*- coding: utf-8 -*-

# Macro to generate the pencil lines onto each of the patches 
# based on the other outlines and on the batton patch design

import FreeCAD as App
import Draft, Part, Mesh
import DraftGeomUtils
import math, os, csv, sys
import numpy
from FreeCAD import Vector, Rotation

sys.path.append(os.path.split(__file__)[0])

#from p7modules.p7wingflatten_barmeshfuncs import findallnodesandpolys, cpolyuvvectorstransF
from p7modules.barmesh.basicgeo import P2, P3, Partition1, Along, I1

doc = App.ActiveDocument

from p7modules.p7wingeval import WingEval
from p7modules.p7wingeval import getemptyfolder, createobjectingroup

R13type = doc.getObject("Group")
wingeval = WingEval(doc, R13type)
urange, vrange, seval, uvals = wingeval.urange, wingeval.vrange, wingeval.seval, wingeval.uvals

from p7modules.p7wingflatten_barmeshfuncs import sliceupatnones
from p7modules.p7wingflatten_barmeshfuncs import MeshBoundary

uvtriangulations = doc.UVTriangulations.OutList
striangulations = doc.STriangulations.OutList
sflattened = doc.SFlattened.OutList
uvpolygons = doc.UVPolygons.OutList

uvpolysamplerate = 20.0  # 2cm (a setting)
uvpolysampleSideErrorFactor = 200.0

uspacing, vspacing = 20, 20

assert len(uvtriangulations) == len(striangulations), (len(uvtriangulations), len(striangulations), len(sflattened))

from p7modules.p7wingflatten_projfuncs import cp2t, cpolyuvvectorstransC, generateTransColumns, findcctriangleinmesh, projectspbarmeshF



def resampleuvseq(ppuvs, ppxyzs, rad):
	i = 0
	radsq = rad*rad
	ppuvsR = [ ]
	assert len(ppuvs) == len(ppxyzs)
	p = ppxyzs[0]
	ppuvsR.append(ppuvs[0])
	worstsevalinterpolation = 0.0
	while i < len(ppxyzs) - 1:
		p1 = ppxyzs[i+1]
		dp1 = p1 - p
		dp1sq = dp1.Lensq()
		if dp1sq < radsq:
			i += 1
			continue
		p0 = ppxyzs[i]
		v = p0 - p1
		qa = v.Lensq()
		qb2 = P3.Dot(dp1, v)
		qc = dp1sq - radsq
		qdq = qb2*qb2 - qa*qc
		assert qdq >= -0.0001
		qs = math.sqrt(max(0, qdq)) / qa
		qm = -qb2 / qa
		lam = qm - qs
		assert 0 <= lam <= 1
		pR = p1 + v*lam
		Dr = (pR - p).Len()
		assert abs(Dr - rad) < 0.001
		ppuvR = Along(lam, ppuvs[i+1], ppuvs[i])
		projacc = (P3(*seval(ppuvR.u, ppuvR.v)) - pR).Len()
		if projacc > 1e-8:    # Just a really lazy binary search.  Don't have time to code better here
			lamD = max(0, lam - 0.1)
			lamU = min(1, lam + 0.1)
			ppuvD = Along(lamD, ppuvs[i+1], ppuvs[i])
			ppuvU = Along(lamU, ppuvs[i+1], ppuvs[i])
			pD = P3(*seval(ppuvD.u, ppuvD.v))
			pU = P3(*seval(ppuvU.u, ppuvU.v))
			DrD = (pD - p).Len()
			DrU = (pU - p).Len()
			assert DrU <= rad and DrD >= rad, (DrU, DrD)
			while lamU - lamD > 1e-7:
				lamM = (lamD + lamU)/2
				ppuvM = Along(lamM, ppuvs[i+1], ppuvs[i])
				pM = P3(*seval(ppuvM.u, ppuvM.v))
				DrM = (pM - p).Len()
				if DrM > rad:
					lamD, ppuvD, pD, DrD = lamM, ppuvM, pM, DrM
				else:
					lamU, ppuvU, pU, DrU = lamM, ppuvM, pM, DrM
			ppuvR = ppuvD if abs(DrD - rad) < abs(DrU - rad) else ppuvU
			print(i, DrU, DrD)
			pR = P3(*seval(ppuvR.u, ppuvR.v))
		worstsevalinterpolation = max(worstsevalinterpolation, (P3(*seval(ppuvR.u, ppuvR.v)) - pR).Len())
		ppuvsR.append(ppuvR)
		p = pR
	#print("worstsevalinterpolation %.5f" % worstsevalinterpolation)  # needs more accuracy
	return ppuvsR


objs = doc.findObjects(Label="SStretch")
if objs and len(objs[0].OutList):
    sstretchlines = objs[0]
else:
    sstretchlines = getemptyfolder(doc, "SStretch")

#uvpolygons = uvpolygons[:1]

if len(sstretchlines.OutList) == 0:
    uvpolysResampled = [ ]
    for ip, uvpolygon in enumerate(uvpolygons):
        ppuvs = [ P2(v.Point.x, v.Point.y)  for v in uvpolygon.Shape.OrderedVertexes ] 
        ppxyzs = [ P3(*seval(p.u, p.v))  for p in ppuvs ]
        rppuvs = resampleuvseq(ppuvs, ppxyzs, uvpolysamplerate)
        uvpolysResampled.append(rppuvs)
        ppxyzsR = [ seval(p.u, p.v)  for p in rppuvs ]
        samplespacings = [ (a - b).Length  for a, b in zip(ppxyzsR[1:], ppxyzsR) ]
        print("samplespacings %.5f %.5f" % (min(samplespacings), max(samplespacings)))
        #ws = createobjectingroup(doc, sstretchlines, "Part::Feature", "t%d"%ip)
        #ws.Shape = Part.makePolygon(ppxyzsR)

    #
    # main loop across the different patches here
    #
    for I in range(len(uvpolygons)):
        uvmesh = uvtriangulations[I]
        surfacemesh = striangulations[I]
        flattenedmesh = sflattened[I]
        assert uvmesh.Mesh.CountFacets == flattenedmesh.Mesh.CountFacets == surfacemesh.Mesh.CountFacets
        name = uvmesh.Name[1:]
        
        xpart, uvtranslistCcolumns = generateTransColumns(uvmesh, flattenedmesh, urange, vrange)
        spsFS = [ ]
        for J in range(len(uvpolygons)):
            #ppxyzsR = [ seval(p.u, p.v)  for p in uvpolysResampled[J] ]
            #ws = createobjectingroup(doc, sstretchlines, "Part::Feature", "s%d"%ip)
            #ws.Shape = Part.makePolygon(ppxyzsR)

            if J != I:
                continue
            spsJ = uvpolysResampled[J]
            spsJF = [ projectspbarmeshF(sp, xpart, uvtranslistCcolumns)  for sp in spsJ ]
            spsFS.extend(sliceupatnones(spsJF))
        for spsS in spsFS:
            ws = createobjectingroup(doc, sstretchlines, "Part::Feature", "w%s_%d"%(name, len(sstretchlines.OutList)))
            ws.Shape = Part.makePolygon([Vector(p[0], p[1], 1.0)  for p in spsS])
            ws.ViewObject.PointColor = (0.0,0.0,1.0)
            ws.ViewObject.LineColor = (0.0,0.0,1.0)

sstretchlines_side = getemptyfolder(doc, "SStretch_side")
agsecNum = 2   # aggregate errors on segments behind and forward
for sstretchline in sstretchlines.OutList:
    slps = [ ]
    pts = [sVV.Point  for sVV in sstretchline.Shape.OrderedVertexes]
    for i in range(len(pts)-1):
        i0 = max(i - agsecNum, 0)
        i1 = min(i + 1 + agsecNum, len(pts)-1)
        vlen = sum((pts[ii+1] - pts[ii]).Length  for ii in range(i0, i1))
        err = vlen/(i1 - i0) - uvpolysamplerate
        p0, p1 = pts[i], pts[i+1]
        v = p1 - p0
        vlen = v.Length
        vperp = Vector(v.y/vlen, -v.x/vlen, 0.0)
        vp = vperp*(err*uvpolysampleSideErrorFactor)
        slps.append(p0 + vp)
        slps.append(p1 + vp)
        
    ws = createobjectingroup(doc, sstretchlines_side, "Part::Feature", "wd_%d"%(len(sstretchlines_side.OutList)))
    ws.Shape = Part.makePolygon(slps)
    ws.ViewObject.PointColor = (1.0,1.0,0.0)
    ws.ViewObject.LineColor = (1.0,1.0,0.0)
